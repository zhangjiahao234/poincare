/*************************************************************************
	> File Name: utils/TickType.h
	> Author: zhangjiahao
	> Mail: zhangjiahao@mycapital.com 
	> Created Time: Thu 23 Nov 2017 02:26:44 PM CST
 ************************************************************************/


#define BidSize	0
#define BidPrice	1
#define AskPrice	2
#define AskSize	3
#define LastPrice	4
#define LastSize	5
#define High	6
#define Low	7
#define Volume	8
#define ClosePrice	9
#define BidOptionComputation	10
#define AskOptionComputation	11
#define LastOptionComputation	12
#define ModelOptionComputation	13
#define OpenTick	14
#define Low13Weeks	15
#define High13Weeks	16
#define Low26Weeks	17
#define High26Weeks	18
#define Low52Weeks	19
#define High52Weeks	20
#define AverageVolume	21
#define OpenInterest	22
#define OptionHistoricalVolatility	23
#define OptionImpliedVolatility	24
#define OptionBidExchange	25
#define OptionAskExchange	26
#define OptionCallOpenInterest	27
#define OptionPutOpenInterest	28
#define OptionCallVolume	29
#define OptionPutVolume	30
#define IndexFuturePremium	31
#define BidExchange	32
#define AskExchange	33
#define AuctionVolume	34
#define AuctionPrice	35
#define AuctionImbalance	36
#define MarkPrice	37
#define BidEFPComputation	38
#define AskEFPComputation	39
#define LastEFPComputation	40
#define OpenEFPComputation	41
#define HighEFPComputation	42
#define LowEFPComputation	43
#define CloseEFPComputation	44
#define LastTimestamp	45
#define Shortable	46
#define FundamentalRatios	47
#define RTVolume    48
#define Halted	49

