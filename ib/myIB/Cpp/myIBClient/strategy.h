#ifndef __STRATEGY_H__
#define __STRATEGY_H__
#include "TickAttrib.h"

void on_tick_price(long TickerId, int field, double price, const TickAttrib& attribs);

void on_tick_size(long TickerId, int field, int size);

void on_time(long time);

#endif