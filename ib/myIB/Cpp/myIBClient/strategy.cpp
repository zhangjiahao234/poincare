#include <ctime>
#include "stdio.h"
#include "strategy.h"
#include "utils/k.h"
#include "utils/TickType.h"

#define KDBLEN 1024

/*
    struct TickAttrib
    {
        bool canAutoExecute;
        bool pastLimit;
        bool preOpen;
    };
*/

static int kdb_handle0 = khpu("localhost",5000,"");
static int kdb_handle1 = khpu("localhost",5001,"");
static int kdb_handle2 = khpu("localhost",5002,"");
static int kdb_handle3 = khpu("localhost",5003,"");
static int kdb_handle4 = khpu("localhost",5004,"");
static int kdb_handle5 = khpu("localhost",5005,"");
static int kdb_handle6 = khpu("localhost",5006,"");
static int kdb_handle7 = khpu("localhost",5007,"");
static int kdb_handle8 = khpu("localhost",5008,"");
static int kdb_handle9 = khpu("localhost",5009,"");

static int kdb_handle15 = khpu("localhost",6005,"");
static int kdb_handle16 = khpu("localhost",6006,"");
static int kdb_handle17 = khpu("localhost",6007,"");
static int kdb_handle18 = khpu("localhost",6008,"");




//K res = (kdb_handle,"ApList:();BpList:();",(K)0);
static char kdb_sql[KDBLEN];



void on_tick_price(long TickerId, int field, double price, const TickAttrib& attribs){
//    printf("Strategy Tick Price. Ticker Id: %ld, Field: %d, Price: %g, CanAutoExecute: %d, PastLimit: %d, PreOpen: %d\n", TickerId, field, price, attribs.canAutoExecute, attribs.pastLimit, attribs.preOpen);
    switch(field){
        case BidPrice:
            printf("ibBidpx:%g\n",price);
            sprintf(kdb_sql,"ibBidpx:%g;",price);
            k(kdb_handle0,kdb_sql,(K)0);
            k(kdb_handle1,kdb_sql,(K)0);
            k(kdb_handle2,kdb_sql,(K)0);
            k(kdb_handle3,kdb_sql,(K)0);
            k(kdb_handle4,kdb_sql,(K)0);
            k(kdb_handle5,kdb_sql,(K)0);
            k(kdb_handle6,kdb_sql,(K)0);
            k(kdb_handle7,kdb_sql,(K)0);
            k(kdb_handle8,kdb_sql,(K)0);
            k(kdb_handle9,kdb_sql,(K)0);



            k(kdb_handle15,kdb_sql,(K)0);
            k(kdb_handle16,kdb_sql,(K)0);
            k(kdb_handle17,kdb_sql,(K)0);
            k(kdb_handle18,kdb_sql,(K)0);

            break;
        case AskPrice:
            printf("ibAskpx:%g\n",price);
            sprintf(kdb_sql,"ibAskpx:%g;",price);
            //k(kdb_handle,kdb_sql,(K)0);
            k(kdb_handle0,kdb_sql,(K)0);
            k(kdb_handle1,kdb_sql,(K)0);
            k(kdb_handle2,kdb_sql,(K)0);
            k(kdb_handle3,kdb_sql,(K)0);
            k(kdb_handle4,kdb_sql,(K)0);
            k(kdb_handle5,kdb_sql,(K)0);
            k(kdb_handle6,kdb_sql,(K)0);
            k(kdb_handle7,kdb_sql,(K)0);
            k(kdb_handle8,kdb_sql,(K)0);
            k(kdb_handle9,kdb_sql,(K)0);


            k(kdb_handle15,kdb_sql,(K)0);
            k(kdb_handle16,kdb_sql,(K)0);
            k(kdb_handle17,kdb_sql,(K)0);
            k(kdb_handle18,kdb_sql,(K)0);

            break;
    }
}

void on_tick_size(long TickerId, int field, int size){
 //   printf("Strategy Tick Size. Ticker Id: %ld, Field: %d, Size: %d\n", TickerId, (int)field, size);
    switch(field){
        case BidSize:
            printf("HGBidSize:%d\n",size);
            sprintf(kdb_sql,"BidSize:%d",size);
            //k(kdb_handle,kdb_sql,(K)0);
            k(kdb_handle0,kdb_sql,(K)0);
            k(kdb_handle1,kdb_sql,(K)0);
            k(kdb_handle2,kdb_sql,(K)0);
            k(kdb_handle3,kdb_sql,(K)0);
            k(kdb_handle4,kdb_sql,(K)0);
            k(kdb_handle5,kdb_sql,(K)0);
            k(kdb_handle6,kdb_sql,(K)0);
            k(kdb_handle7,kdb_sql,(K)0);
            k(kdb_handle8,kdb_sql,(K)0);
            k(kdb_handle9,kdb_sql,(K)0);
            break;
        case AskSize:
            printf("HGAskSize:%d\n",size);
            sprintf(kdb_sql,"AskSize:%d",size);
           // k(kdb_handle,kdb_sql,(K)0);
            k(kdb_handle0,kdb_sql,(K)0);
            k(kdb_handle1,kdb_sql,(K)0);
            k(kdb_handle2,kdb_sql,(K)0);
            k(kdb_handle3,kdb_sql,(K)0);
            k(kdb_handle4,kdb_sql,(K)0);
            k(kdb_handle5,kdb_sql,(K)0);
            k(kdb_handle6,kdb_sql,(K)0);
            k(kdb_handle7,kdb_sql,(K)0);
            k(kdb_handle8,kdb_sql,(K)0);
            k(kdb_handle9,kdb_sql,(K)0);
            break;
    }
}

void on_time(long time){
    time_t t = ( time_t)time;
    struct tm * timeinfo = localtime ( &t);
    //printf("Strategy The current date/time is: %s", asctime( timeinfo));
}
